console.log("s18 Activity");

let trainer = {
	name: "brock",
	age: 20,
	pokemon: [`onix`, `dragonite`, `pikachu`],
	friends: {
			name: 'ash',
			age:  '23'	
	},
	talk: function() {
		console.log('Pikachu! I choose you');
	}
}
console.log(trainer[`name`], trainer[`age`]);
console.log(trainer.name,trainer.age);
trainer.talk();


function Pokemon(name, level) {
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = 2 * level;

	//methods
	this.tackle = function(target) {
		console.log(`${this.name} tackled ${target.name}`);
		target.health = target.health - target.attack;
		if(target.health <= 0){
			target.faint();
		}

	};
	this.faint = function() {
		console.log(`${this.name} fainted.`)
	}
}

let onix = new Pokemon("Onix", 30);
let dragonite = new Pokemon("Dragonite", 50);

onix.tackle(dragonite);





